## 0.5.1

- Fixed issue in `Topological Sort` that would cause duplicates when one node depended on another node multiple times.


## 0.5.0

- Changed `Natural Sort` to a malleable VI

## 0.4.3

- Fixed Bug that caused GUIDs to be missing 2 bytes

## 0.4.2

- Fixed Misc icons

## 0.4.1

- Added `Show Scrollbars if Needed.vi`
- Added error terminals to `Center Controls in Pane.vi`

## 0.4.0

- Added `Tolopolical Sort`
- Removed duplicate VI `Array of Enum Values` (Equivalent to Enumerate Enum)


## 0.3.0

- Added `GUIDv4`
- Added `Enumerate Enum`

## 0.2.0

- Reworked enable if/gray if to be less confusing
- Added `Array of Enum Values.vim`
- Reformatted readme.

## 0.1.2

- Added several array helper VIs

## 0.1.1

- Updated Readme and Relinked VIs

## 0.1.0

- Initial Release.
