﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*W!!!*Q(C=\&gt;5R4BN"&amp;-&lt;RDSB)+8&amp;0%8Q"CJ?+WL6&amp;A9MU\HA&amp;H3OXS!*E/C43P$YJ9EY1_1KPY!+_AE_!:0Y\/^GA1/1G3)H%L'&gt;N@T-\]`0M;#W6]E&amp;[L]WW-PR$G4W-/&lt;@NZ&gt;S=3P]3T8;P^];`NZ?XNPXQ]0LIK'PPDP:&lt;=`XH`&gt;0R#_W&lt;H\X/^M^_T6^@4]9@TL\.HL50SQ^KW___XH8N,Z;(6L[F[+W]F@_\H'`&gt;Z+__VX@50)CI31WK5UX.F*ME4`)E4`)E4`)A$`)A$`)A$X)H&gt;X)H&gt;X)H&gt;X)D.X)D.X)D.`*SE)N=Z#+(F%S?4*1-GAS1&gt;);CZ*2Y%E`C34R]6?**0)EH]31?OCDR**\%EXA3$Z=J]33?R*.Y%A^$&amp;5G7ARR0YG&amp;Y":\!%XA#4_"B3A7?!""-&amp;AQ=$!*$170Q)@!%HM$$2Q7?Q".Y!E`AI6G"*`!%HM!4?,CEL%I548/1YW%9/2\(YXA=D_.B;$E?R_.Y()`D94IZ(M@D)*Q*H=%BS,H)[?"]=4S/BT=Z(M@D?"S0Y['JX#%P+^.IGI-=D_%R0)&lt;(]"A?BJ$B-4S'R`!9(I;6Y4%]BM@Q'"[GEO%R0)&lt;(A"C4-LW-Q9Q,D5Z'9(BYF&gt;VCZ3Z&amp;E6AZJ,JZ64?F[G:4X53KGU0VJKP?4.7&lt;J,LYKIOKOFCKC[$[YV42KBD6361P&lt;DJKT8F&amp;86)8V$FV3BV2"^1_N&gt;&gt;=_J=\LN&gt;LL69L,:&gt;,,29,T?&gt;T4;&gt;4D59D$19$^@N^^8K^\D(QE;.\),40J8.J=G5XX[]_(2^=HOT^O$SZHVR]/:Z=X.Z0;$NYZ@`A@_$:K(&gt;[/DZL^!D:_Y10!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI_IconEditor" Type="Str">49 55 48 49 56 48 49 48 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 4 143 1 100 1 100 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 4 35 0 0 0 0 0 0 0 0 0 0 4 6 0 40 0 0 4 0 0 0 3 192 0 0 0 0 0 10 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 102 102 51 68 68 68 153 102 51 153 204 255 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 153 204 255 0 0 0 0 0 0 0 0 0 153 204 255 153 204 255 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 102 102 51 255 255 0 102 51 0 255 153 51 153 102 51 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 0 0 0 153 204 255 153 204 255 153 204 255 0 0 0 153 204 255 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 204 204 0 204 204 0 102 51 0 204 102 51 204 102 51 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 0 0 0 153 204 255 0 0 0 153 204 255 0 0 0 153 204 255 0 0 0 153 204 255 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 102 102 51 153 153 0 255 51 51 153 51 0 153 102 51 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 0 0 0 153 204 255 0 0 0 153 204 255 0 0 0 153 204 255 0 0 0 153 204 255 0 0 0 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 0 51 255 51 51 153 0 51 153 204 255 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 0 0 0 153 204 255 153 204 255 0 0 0 153 204 255 153 204 255 0 0 0 153 204 255 0 0 0 153 204 255 153 204 255 153 204 255 0 0 0 153 204 255 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 102 102 51 51 102 102 102 102 51 153 204 255 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 0 0 0 153 204 255 153 204 255 0 0 0 153 204 255 153 204 255 0 0 0 153 204 255 153 204 255 0 0 0 0 0 0 0 0 0 0 0 0 153 204 255 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 51 51 102 51 102 153 51 51 102 153 204 255 153 204 255 153 204 255 0 0 0 0 0 0 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 153 204 255 68 68 68 153 204 255 153 204 255 153 204 255 153 204 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 9 67 108 105 112 98 111 97 114 100 100 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 0 10 1 0

</Property>
	<Item Name="Application" Type="Folder">
		<Item Name="Caller&apos;s VI Reference.vi" Type="VI" URL="../Caller&apos;s VI Reference.vi"/>
		<Item Name="Center Controls in Pane.vi" Type="VI" URL="../Center Controls in Pane.vi"/>
		<Item Name="Current VI&apos;s Reference.vi" Type="VI" URL="../Current VI&apos;s Reference.vi"/>
		<Item Name="Enabled If.vim" Type="VI" URL="../Enabled If.vim"/>
		<Item Name="Get Executable Version.vi" Type="VI" URL="../Get Executable Version.vi"/>
		<Item Name="Gray If.vim" Type="VI" URL="../Gray If.vim"/>
		<Item Name="GUIDv4.vi" Type="VI" URL="../GUIDv4.vi"/>
		<Item Name="Level&apos;s VI Reference.vi" Type="VI" URL="../Level&apos;s VI Reference.vi"/>
		<Item Name="Origin at Top Left.vi" Type="VI" URL="../Origin at Top Left.vi"/>
		<Item Name="Show Scrollbars if Needed.vi" Type="VI" URL="../Show Scrollbars if Needed.vi"/>
		<Item Name="Top Level VI Reference.vi" Type="VI" URL="../Top Level VI Reference.vi"/>
		<Item Name="VI Reference.vi" Type="VI" URL="../VI Reference.vi"/>
	</Item>
	<Item Name="Array" Type="Folder">
		<Item Name="Average.vim" Type="VI" URL="../Average.vim"/>
		<Item Name="Delete Elements.vim" Type="VI" URL="../Delete Elements.vim"/>
		<Item Name="Enumerate Enum.vim" Type="VI" URL="../Enumerate Enum.vim"/>
		<Item Name="Filter 1D Array.vim" Type="VI" URL="../Filter 1D Array.vim"/>
		<Item Name="Index Elements.vim" Type="VI" URL="../Index Elements.vim"/>
		<Item Name="Natural Sort.vim" Type="VI" URL="../Natural Sort.vim"/>
		<Item Name="Remove Duplicates.vim" Type="VI" URL="../Remove Duplicates.vim"/>
		<Item Name="Search Array.vim" Type="VI" URL="../Search Array.vim"/>
		<Item Name="Sort 1D Array.vim" Type="VI" URL="../Sort 1D Array.vim"/>
		<Item Name="Topological Sort.vi" Type="VI" URL="../Topological Sort.vi"/>
	</Item>
	<Item Name="Error" Type="Folder">
		<Item Name="Clear Error.vim" Type="VI" URL="../Clear Error.vim"/>
	</Item>
	<Item Name="File" Type="Folder">
		<Item Name="Append Text to File.vi" Type="VI" URL="../Append Text to File.vi"/>
		<Item Name="Create Directory Chain.vi" Type="VI" URL="../Create Directory Chain.vi"/>
		<Item Name="Make String Filesafe.vi" Type="VI" URL="../Make String Filesafe.vi"/>
		<Item Name="Non Repeating Plot Color.vi" Type="VI" URL="../Non Repeating Plot Color.vi"/>
		<Item Name="Replace File Extension.vi" Type="VI" URL="../Replace File Extension.vi"/>
	</Item>
	<Item Name="Timing" Type="Folder">
		<Item Name="Last Reset.vi" Type="VI" URL="../Last Reset.vi">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="Milliseconds Since Last Call.vim" Type="VI" URL="../Milliseconds Since Last Call.vim"/>
		<Item Name="Milliseconds Since Last Reset.vim" Type="VI" URL="../Milliseconds Since Last Reset.vim"/>
		<Item Name="Wait.vim" Type="VI" URL="../Wait.vim"/>
	</Item>
</Library>
